package com.example.acer.tipcalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText bill, nPeople;
    private SeekBar percent;
    private TextView tipDisplay, totalDisplay, percentText;
    int value;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bill = (EditText) findViewById(R.id.billTxt);
        nPeople = (EditText) findViewById(R.id.peopleTxt);
        percent = (SeekBar) findViewById(R.id.seekBar);
        tipDisplay = (TextView) findViewById(R.id.tipDisplay);
        totalDisplay = (TextView) findViewById(R.id.totalDisplay);
        percentText = (TextView) findViewById(R.id.percentText);

        value = percent.getProgress();
        percentText.setText(value + "%");

        bill.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                calculate();
            }
        });

        nPeople.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                calculate();
            }
        });

        percent.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                value = percent.getProgress();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                percentText.setText(value + "%");
                calculate();
            }
        });
    }
    public void calculate()
    {
        float b, p, tipD, totalD;
        try {
            b = Integer.parseInt(bill.getText().toString());
            p = Integer.parseInt(nPeople.getText().toString());

            tipD = (b/p)*((float)value/100);
            totalD = (b/p) + tipD;
            tipDisplay.setText(tipD + "");
            totalDisplay.setText(totalD + "");
        }
        catch(Exception e)
        {
            tipDisplay.setText("0.0");
            totalDisplay.setText("0.0");
        }

    }
}




